import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-calculos-binario',
  templateUrl: './calculos-binario.component.html',
  styleUrls: ['./calculos-binario.component.scss']
})
export class CalculosBinarioComponent implements OnInit {

  formGroup: FormGroup = new FormGroup({
    num1: new FormControl('', [isInvalid]),
    num2: new FormControl('', [isInvalid]),
    operador: new FormControl('+')
  });
  operadores: Array<{id: string, descricao: string}> = [
    {
      id: '+',
      descricao: 'Somar'
    },
    {
      id: '-',
      descricao: 'Subtrair'
    },
    {
      id: '/',
      descricao: 'Dividir'
    },
    {
      id: '*',
      descricao: 'Multiplicar'
    }
  ];

  resultado = '';
  constructor() { }

  ngOnInit(): void {
  }

  calcular() {
    if (this.formGroup.valid) {
      const num1 = parseInt(this.formGroup.get('num1').value, 2);
      const num2 = parseInt(this.formGroup.get('num2').value, 2);
      let result = 0;
      switch (this.formGroup.get('operador').value) {
        case '+':
          result = num1 + num2;
          break;
        case '-':
          result = num1 - num2;
          break;
        case '/':
          result = num1 / num2;
          break;
        case '*':
          result = num1 * num2;
          break;
      }
      this.resultado = result.toString(2);
    }
  }

}

function isInvalid(control: AbstractControl): any {
  const reg = new RegExp(/^[0-1]+$/);
  if (reg.test(control.value)) {
    return null;
  } else {
    return 'invalid';
  }
}
