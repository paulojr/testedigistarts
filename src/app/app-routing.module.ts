import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ElementosUnicosComponent} from './elementos-unicos/elementos-unicos.component';
import {CalculosBinarioComponent} from './calculos-binario/calculos-binario.component';
import {MenuComponent} from './menu/menu.component';

const routes: Routes = [
  {
    path: '',
    component: MenuComponent
  },
  {
    path: 'elementos-unicos',
    component: ElementosUnicosComponent
  },
  {
    path: 'calculos-binario',
    component: CalculosBinarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
