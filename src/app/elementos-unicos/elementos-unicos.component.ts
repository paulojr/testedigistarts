import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl} from '@angular/forms';

@Component({
  selector: 'app-elementos-unicos',
  templateUrl: './elementos-unicos.component.html',
  styleUrls: ['./elementos-unicos.component.scss']
})
export class ElementosUnicosComponent implements OnInit {

  numeroControl: FormControl = new FormControl('', [isInvalid]);
  itens: Array<number> = [];

  constructor() { }

  get limiteInvalido() {
    return (parseInt(this.numeroControl.value, 10) < -1000 || parseInt(this.numeroControl.value, 10) > 1000);
  }

  get numeroInvalido() {
    const reg = new RegExp(/[a-zA-Z]/g);
    return reg.test(this.numeroControl.value);
  }

  ngOnInit(): void {
  }

  addItem() {
    if (this.numeroControl.valid) {
      const duplicado = this.itens.find(item => item === this.numeroControl.value);
      if (!duplicado) {
        this.itens.push(this.numeroControl.value);
      }
      this.itens = this.itens.sort((a, b) => {
        return a - b;
      });
      this.numeroControl.setValue('');
    }
  }

}

function isInvalid(control: AbstractControl): any {
  const reg = new RegExp(/[0-9]/g);
  if (reg.test(control.value) && (parseInt(control.value, 10) >= -1000 && parseInt(control.value, 10) <= 1000)) {
    return null;
  } else {
    return 'invalid';
  }
}
